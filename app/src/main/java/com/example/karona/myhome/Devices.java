package com.example.karona.myhome;

import com.google.firebase.database.Exclude;

import java.util.HashMap;
import java.util.Map;

public class Devices {
    public String id;
    public String userPhone;
    public String deviceName;
    public String description;
    public String value;
    public String relayValue;
    public String created_at;

    // Default constructor required for calls to
    // DataSnapshot.getValue(User.class)
    public Devices() {    }

    public Devices(String id,String userPhone,String deviceName,String value,String description,String relayValue,String created_at) {
        this.id = id;
        this.userPhone = userPhone;
        this.deviceName = deviceName;
        this.value = value;
        this.description = description;
        this.relayValue = relayValue;
        this.created_at = created_at;
    }
    public void setId(String id){ this.id = id; }
    public String getId(){ return id;}

    public void setUserPhone(String userPhone) { this.userPhone = userPhone; }
    public String getUserPhone(){ return userPhone; }

    public void setDeviceName(String deviceName) { this.deviceName = deviceName; }
    public String getDeviceName() {
        return deviceName;
    }

    public void setValue(String value){ this.value = value; }
    public String getValue() {
        return value;
    }

    public void setDescription(String description){ this.description = description; }
    public String getDescription() {
        return description;
    }

    public void setRelayValue(String relayValue){ this.relayValue = relayValue; }
    public String getRelayValue() {
        return relayValue;
    }

    public void setCreated_at(String created_at){ this.created_at = created_at; }
    public String getCreated_at() { return created_at; }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();
        result.put("DeviceName", deviceName);
        result.put("Value",value );
        result.put("Description", description);
        result.put("RelayValue",relayValue );
        return result;
    }
}
