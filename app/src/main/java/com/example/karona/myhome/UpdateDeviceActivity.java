package com.example.karona.myhome;

import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class UpdateDeviceActivity extends AppCompatActivity {
    String deviceid;
    private static final String TAG_PID = "deviceid";
    private TextInputLayout EditTextdeviceName, EditTextdescription;
    private Spinner ledvalue, relayValue;
    private String strDeviceID = "", strUserphone = "",strDeviceName = "",strValue = "",strDecription = "",strRelayValue = "",strCreated_at = "";
    private String strDeviceName1 = "",strValue1 = "",strDecription1 = "",strRelayValue1 = "";
    private List<String> values;
    private PreferenceHelper preferenceHelper;
    private ParseContent parseContent;
    private final int RegTask = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_device);
        EditTextdeviceName = (TextInputLayout) findViewById(R.id.device_la);
        EditTextdescription = (TextInputLayout) findViewById(R.id.description_la);
        ledvalue = (Spinner) findViewById(R.id.spinner_value);
        relayValue = (Spinner) findViewById(R.id.spinner_ralay);
        values = new ArrayList<>();
        values.add("Relay 0");
        values.add("Relay 1");
        values.add("Relay 2");
        values.add("Relay 3");
        values.add("Relay 4");
        values.add("Relay 5");
        values.add("Relay 6");
        values.add("Relay 7");
        values.add("Relay 8");
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, values);
        relayValue.setAdapter(spinnerArrayAdapter);
        // getting product details from intent
        Intent i = getIntent();
        // getting product id (pid) from intent
        deviceid = i.getStringExtra(TAG_PID);
        preferenceHelper = new PreferenceHelper(this);
        parseContent = new ParseContent(this);

        showDevice();
        // GET Value form controller

        ledvalue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                strValue1 = ledvalue.getSelectedItem().toString().trim();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                strValue1 = ledvalue.getSelectedItem().toString().trim();
            }
        });


        relayValue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                strRelayValue1 = relayValue.getSelectedItem().toString().trim();
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                strRelayValue1 = relayValue.getSelectedItem().toString().trim();
            }
        });
        //
        Button btnSave = (Button) findViewById(R.id.buttonSave);
        btnSave.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SaveDevice();
                //Toast.makeText(UpdateDeviceActivity.this, "Device has been updated successfully!", Toast.LENGTH_SHORT).show();
                //Intent newActivity = new Intent(UpdateDeviceActivity.this, DeviceActivity.class);
                //startActivity(newActivity);
                finish();
            }
        });
    }

    public void showDevice() {
        RequestQueue requestQueue = Volley.newRequestQueue(UpdateDeviceActivity.this);
        StringRequest stringRequest = new StringRequest(Request.Method.POST, ConnectServer.ServiceType.EDIT_DEVICE + deviceid, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    Log.d("response", response + "");
                    //JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = new JSONArray(response);
                    if (jsonArray.length() > 0) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(0);
                        strDeviceID = jsonObject1.getString("deviceid");
                        strUserphone = jsonObject1.getString("userphone");
                        strDeviceName = jsonObject1.getString("devicename");
                        EditTextdeviceName.getEditText().setText(strDeviceName + "");
                        strValue = jsonObject1.getString("value");
                        strDecription = jsonObject1.getString("description");
                        EditTextdescription.getEditText().setText(strDecription + "");
                        strRelayValue = jsonObject1.getString("relayvalue");
                        relayValue.setSelection(values.indexOf(strRelayValue));
                        strCreated_at = jsonObject1.getString("created_at");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.i("HiteshURLerror", "" + error);
            }
        });

        requestQueue.add(stringRequest);

    }


    public void SaveDevice() {
        if (!ConnectServerUtils.isNetworkAvailable(UpdateDeviceActivity.this)) {
            Toast.makeText(UpdateDeviceActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
        }
        strDeviceName1 = EditTextdeviceName.getEditText().getText().toString().trim();
        strDecription1 = EditTextdescription.getEditText().getText().toString().trim();
        Log.e("Start Record", "==================================================\n");
        Log.d("DeviceID", strDeviceID + "");
        Log.d("Device", strDeviceName1 + "");
        Log.d("Value", strValue1 + "");
        Log.d("Description", strDecription1 + "");
        Log.d("Relay Value", strRelayValue1 + "");
        Log.e("End Record", "==================================================\n");

        final HashMap<String, String> map = new HashMap<>();
        map.put(ConnectServer.editDevice_Params.DEVICEID, strDeviceID.toString());
        map.put(ConnectServer.editDevice_Params.DEVICENAME, strDeviceName1.toString());
        map.put(ConnectServer.editDevice_Params.VALUE, strValue1.toString());
        map.put(ConnectServer.editDevice_Params.DESCRIPTION, strDecription1.toString());
        map.put(ConnectServer.editDevice_Params.RELAYVALUE, strRelayValue1.toString());
        new AsyncTask<Void, Void, String>() {
            protected String doInBackground(Void[] params) {
                String response = "";
                try {
                    HttpRequest req = new HttpRequest(ConnectServer.ServiceType.UPDATE_DEVICE);
                    response = req.prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                } catch (Exception e) {
                    response = e.getMessage();
                }
                return response;
            }

            protected void onPostExecute(String result) {
                //do something with response
                Log.d("on Post Execute:", result);
                onTaskCompleted(result, RegTask);
            }
        }.execute();
    }

    private void onTaskCompleted(String response, int task) {
        Log.d("responsejson", response.toString());
        ConnectServerUtils.removeSimpleProgressDialog();  //will remove progress dialog
        switch (task) {
            case RegTask:
                if (parseContent.isSuccess(response)) {
                    parseContent.saveInfo(response);
                    Toast.makeText(UpdateDeviceActivity.this, "Device has been updated successfully!", Toast.LENGTH_SHORT).show();
                    EditTextdeviceName.getEditText().setText("");
                    EditTextdescription.getEditText().setText("");
                } else {
                    Toast.makeText(UpdateDeviceActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
                }
        }
    }
}
