package com.example.karona.myhome;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.SingleLineTransformationMethod;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.firebase.client.AuthData;
import com.firebase.client.Firebase;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class StartActivity extends AppCompatActivity {
    private Button btnwelcome;

    private ParseContent parseContent;
    private PreferenceHelper preferenceHelper;
    private final int RegTask = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        preferenceHelper = new PreferenceHelper(this);
        parseContent = new ParseContent(this);

        if(preferenceHelper.getIsLogin()){
            Intent intentMain = new Intent(StartActivity.this, MainActivity.class);
            startActivity(intentMain);
            finish();
        }
        btnwelcome = findViewById(R.id.buttonWelcome);
        findViewById(R.id.buttonWelcome).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentWelcome = new Intent(StartActivity.this, SignInActivity.class);
                startActivity(intentWelcome);
                finish();
            }
        });

    }
}
