package com.example.karona.myhome;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.ArrayList;
import java.util.List;

public class DeviceActivity extends AppCompatActivity {
    List<Devices> deviceList;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager recyclerViewlayoutManager;
    RecyclerView.Adapter recyclerViewadapter;
    ProgressBar progressBar;
    String JSON_ID = "deviceid";
    String JSON_PHONE_NUMBER = "userphone";
    String JSON_NAME = "devicename";
    String JSON_VALUE = "value";
    String JSON_DESCRIPTION = "description";
    String JSON_RELAY_STATUS = "relayvalue";
    String JSON_DATETIME = "created_at";
    private boolean isUserScrolling = false;
    private boolean isListGoingUp = true;

    JsonArrayRequest jsonArrayRequest ;
    RequestQueue requestQueue ;
    private PreferenceHelper preferenceHelper;
    SwipeRefreshLayout mSwipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_device);
        mSwipeRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.RefreshLayout);
        preferenceHelper = new PreferenceHelper(this);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //SubjectFullFormListView.smoothScrollToPosition(4);
                Log.i("New UI Add Device", "Manager has been clicked");
                Intent intent = new Intent(DeviceActivity.this, AddDeviceActivity.class);
                startActivity(intent);
            }
        });

        /*GET*/
        //-----------------------

        deviceList = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView_item);
        progressBar = (ProgressBar) findViewById(R.id.progressBar1);
        recyclerView.setHasFixedSize(true);
        recyclerViewlayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(recyclerViewlayoutManager);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                JSON_DATA_WEB_CALL();
            }
        });
        progressBar.setVisibility(View.VISIBLE);
        //JSON_DATA_WEB_CALL();

    }
    public void JSON_DATA_WEB_CALL(){
        jsonArrayRequest = new JsonArrayRequest(ConnectServer.ServiceType.GET_DEVICE+"?userphone="+ preferenceHelper.getUsername().toString(),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        progressBar.setVisibility(View.GONE);
                        mSwipeRefreshLayout.setRefreshing(false);
                        JSON_PARSE_DATA_AFTER_WEBCALL(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }

    public void JSON_PARSE_DATA_AFTER_WEBCALL(JSONArray array){
        deviceList.clear();
        for(int i = 0; i<array.length(); i++) {
            Devices device = new Devices();
            JSONObject json = null;
            try {
                json = array.getJSONObject(i);

                device.setId(json.getString(JSON_ID));
                device.setUserPhone(json.getString(JSON_PHONE_NUMBER));
                device.setDeviceName(json.getString(JSON_NAME));
                device.setValue(json.getString(JSON_VALUE));
                device.setDescription(json.getString(JSON_DESCRIPTION));
                device.setRelayValue(json.getString(JSON_RELAY_STATUS));
                device.setCreated_at(json.getString(JSON_DATETIME));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            deviceList.add(device);
        }

        recyclerViewadapter = new RecyclerViewAdapter(deviceList, this);
        recyclerView.setAdapter(recyclerViewadapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mSwipeRefreshLayout.setRefreshing(true);
        JSON_DATA_WEB_CALL();

    }
}