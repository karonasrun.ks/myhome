package com.example.karona.myhome;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {
    List<Devices> deviceList;
    RecyclerView recyclerView;
    RecyclerView.LayoutManager recyclerViewlayoutManager;
    RecyclerView.Adapter recyclerViewMainAdapter;
    ProgressBar progressBar;
    String JSON_ID = "deviceid";
    String JSON_PHONE_NUMBER = "userphone";
    String JSON_NAME = "devicename";
    String JSON_VALUE = "value";
    String JSON_DESCRIPTION = "description";
    String JSON_RELAY_STATUS = "relayvalue";
    String JSON_DATETIME = "created_at";
    private boolean isUserScrolling = false;
    private boolean isListGoingUp = true;
    ImageView imageView;
    JsonArrayRequest jsonArrayRequest ;
    RequestQueue requestQueue ;
    private PreferenceHelper preferenceHelper;
    SwipeRefreshLayout mSwipeRefreshLayout;
    TextView tvphone,tvusername;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        mSwipeRefreshLayout=(SwipeRefreshLayout)findViewById(R.id.RefreshLayout);


        preferenceHelper = new PreferenceHelper(this);
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        View headerView = navigationView.getHeaderView(0);
        TextView navPhone = (TextView) headerView.findViewById(R.id.textViewPhone);
        TextView navUsername = (TextView) headerView.findViewById(R.id.textViewUser);
        navUsername.setText(preferenceHelper.getName()+" "+preferenceHelper.getHobby());
        navPhone.setText(preferenceHelper.getUsername());

        deviceList = new ArrayList<>();
        imageView = (ImageView) findViewById(R.id.imageView);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view_item_main);
        recyclerView.setHasFixedSize(true);
        recyclerViewlayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(recyclerViewlayoutManager);
        JSON_DATA_WEB_CALL();
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                JSON_DATA_WEB_CALL();
            }
        });

    }
    // -----------------------
    public void JSON_DATA_WEB_CALL(){
        jsonArrayRequest = new JsonArrayRequest(ConnectServer.ServiceType.GET_DEVICE+"?userphone="+ preferenceHelper.getUsername().toString(),
                new Response.Listener<JSONArray>() {
                    @Override
                    public void onResponse(JSONArray response) {
                        mSwipeRefreshLayout.setRefreshing(false);
                        JSON_PARSE_DATA_AFTER_WEBCALL(response);
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                    }
                });

        requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jsonArrayRequest);
    }

    public void JSON_PARSE_DATA_AFTER_WEBCALL(JSONArray array){
        deviceList.clear();
        for(int i = 0; i<array.length(); i++) {
            Devices device = new Devices();
            JSONObject json = null;
            try {
                json = array.getJSONObject(i);

                device.setId(json.getString(JSON_ID));
                device.setUserPhone(json.getString(JSON_PHONE_NUMBER));
                device.setDeviceName(json.getString(JSON_NAME));
                device.setValue(json.getString(JSON_VALUE));
                device.setDescription(json.getString(JSON_DESCRIPTION));
                device.setRelayValue(json.getString(JSON_RELAY_STATUS));
                device.setCreated_at(json.getString(JSON_DATETIME));

            } catch (JSONException e) {
                e.printStackTrace();
            }
            deviceList.add(device);
        }

        recyclerViewMainAdapter = new RecyclerViewMainAdapter(deviceList, this);
        recyclerView.setAdapter(recyclerViewMainAdapter);
    }
    // -----------------------

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        if (id == R.id.nav_devices) {
            Log.i("nav device", "Device has been clicked");
            Intent intentDevice = new Intent(MainActivity.this, DeviceActivity.class);
            startActivity(intentDevice);
        }else if(id==R.id.nav_manage) {
            Log.i("nav device", "Manager has been clicked");
            Intent intent = new Intent(MainActivity.this, SettingActivity.class);
            startActivity(intent);
        }else if(id==R.id.nav_about) {
            Log.i("nav device", "Manager has been clicked");
            Intent intent = new Intent(MainActivity.this, AboutActivity.class);
            startActivity(intent);
        }else if(id==R.id.nav_signup){
                showPopup();
        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
    // first step helper function
    private void showPopup() {
        AlertDialog.Builder alert = new AlertDialog.Builder(MainActivity.this);
        alert.setMessage("តើលោកអ្នកចង់ចាកចេញពីកម្មវិធីំមែនទេ?")
                .setPositiveButton("យល់ព្រម", new DialogInterface.OnClickListener()                 {
                    public void onClick(DialogInterface dialog, int which) {
                        logout(); // Last step. Logout function
                    }
                }).setNegativeButton("បោះបង់", null);
        AlertDialog alert1 = alert.create();
        alert1.show();
    }
    private void logout(){
        preferenceHelper.putIsLogin(false);
        Intent intent = new Intent(MainActivity.this,SignInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
