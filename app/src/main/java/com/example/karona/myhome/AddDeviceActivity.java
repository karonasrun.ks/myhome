package com.example.karona.myhome;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

public class AddDeviceActivity extends AppCompatActivity {
    private TextInputLayout EditTextdeviceName,EditTextdescription;
    private Spinner ledvalue,relayValue;
    private String strDeviceName,strValue,strDescription,strRelayValue;
    private Button buttonSave;
    private PreferenceHelper preferenceHelper;
    private ParseContent parseContent;
    private final int RegTask = 1;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_device);
        preferenceHelper = new PreferenceHelper(this);
        parseContent = new ParseContent(this);

        EditTextdeviceName = (TextInputLayout) findViewById(R.id.device_la);
        EditTextdescription = (TextInputLayout)findViewById(R.id.description_la);
        ledvalue = (Spinner)findViewById(R.id.spinner_value);
        relayValue = (Spinner)findViewById(R.id.spinner_ralay);

        ledvalue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {
                strValue = ledvalue.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                strValue = ledvalue.getSelectedItem().toString();
            }

        });


        relayValue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView, View selectedItemView, int position, long id) {

                strRelayValue = relayValue.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                strRelayValue = relayValue.getSelectedItem().toString();
            }

        });


        buttonSave = (Button) findViewById(R.id.buttonSave);
        buttonSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                strDeviceName = EditTextdeviceName.getEditText().getText().toString();
                strDescription = EditTextdescription.getEditText().getText().toString();
                if(strDeviceName.isEmpty()){
                } else if(strDescription.isEmpty()){
                }else{
                    Log.i("","Get User phone number:"+preferenceHelper.getUsername());
                    Log.i("", "Get data EditText DeviceName:" + strDeviceName);
                    Log.i("", "Get data ComboBox Status:" + strValue);
                    Log.i("", "Get data EditText Description:" + strDescription);
                    Log.i("", "Get data ComboBox Relay:" + strRelayValue);
                    //Call WebService
                    try {
                        InputMethodManager imm = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
                        register();
                    } catch (IOException e) {
                        e.printStackTrace();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }

            }
        });
    }
    private void register() throws IOException, JSONException {
        if (!ConnectServerUtils.isNetworkAvailable(AddDeviceActivity.this)) {
            Toast.makeText(AddDeviceActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        ConnectServerUtils.showSimpleProgressDialog(AddDeviceActivity.this);
        final HashMap<String, String> map = new HashMap<>();
        map.put(ConnectServer.addDevice_Params.USERPHONE, preferenceHelper.getUsername().toString());
        map.put(ConnectServer.addDevice_Params.DEVICENAME, strDeviceName.toString());
        map.put(ConnectServer.addDevice_Params.VALUE, strValue.toString());
        map.put(ConnectServer.addDevice_Params.DESCRIPTION, strDescription.toString());
        map.put(ConnectServer.addDevice_Params.RELAYVALUE, strRelayValue.toString());
        new AsyncTask<Void, Void, String>() {
            protected String doInBackground(Void[] params) {
                String response = "";
                try {
                    HttpRequest req = new HttpRequest(ConnectServer.ServiceType.ADD_DEVICE);
                    response = req.prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                } catch (Exception e) {
                    response = e.getMessage();
                }
                return response;
            }

            protected void onPostExecute(String result) {
                //do something with response
                Log.d("newwwss", result);
                onTaskCompleted(result, RegTask);
            }
        }.execute();
    }

    private void onTaskCompleted(String response, int task) {
        Log.d("responsejson", response.toString());
        ConnectServerUtils.removeSimpleProgressDialog();  //will remove progress dialog
        switch (task) {
            case RegTask:
                if (parseContent.isSuccess(response)) {
                    parseContent.saveInfo(response);
                    Toast.makeText(AddDeviceActivity.this, "Device has been created successfully!", Toast.LENGTH_SHORT).show();
                    EditTextdeviceName.getEditText().setText("");
                    EditTextdescription.getEditText().setText("");
                } else {
                    Toast.makeText(AddDeviceActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
                }
        }
    }
}