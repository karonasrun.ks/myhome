package com.example.karona.myhome;

public class ConnectServer {
    // web service url constants
    public class ServiceType {
        public static final String BASE_URL = "https://karona3it.000webhostapp.com/apimobile/";
        public static final String LOGIN = BASE_URL + "userlogin.php";
        public static final String REGISTER =  BASE_URL + "userregistration.php";
        public static final String GET_DEVICE = BASE_URL + "get_devices.php";
        public static final String ADD_DEVICE = BASE_URL + "add_device.php";
        public static final String EDIT_DEVICE = BASE_URL + "edit_device.php?deviceid=";
        public static final String UPDATE_DEVICE = BASE_URL + "update_device.php";
        public static final String UPDATE_RALAY = BASE_URL + "update_relay.php";
        public static final String DELETE_DEVICE = BASE_URL + "delete_device.php";
    }
    // webservice key constants
    public class Params {
        public static final String NAME = "name";
        public static final String HOBBY = "hobby";
        public static final String USERNAME = "username";
        public static final String PASSWORD = "password";
    }

    public class addDevice_Params{
        public static final String USERPHONE = "userphone";
        public static final String DEVICENAME = "devicename";
        public static final String VALUE = "value";
        public static final String DESCRIPTION = "description";
        public static final String RELAYVALUE = "relayvalue";
    }

    public class editDevice_Params{
        public static final String DEVICEID = "deviceid";
        public static final String DEVICENAME = "devicename";
        public static final String VALUE = "value";
        public static final String DESCRIPTION = "description";
        public static final String RELAYVALUE = "relayvalue";
    }

    public class deleteDevice_Params{
        public static final String DEVICEID = "deviceid";
    }

    public class getRelay_Params{
        public static final String DEVICEID = "deviceid";
        public static final String DEVICENAME = "devicename";
        public static final String VALUE = "value";
    }

    public class editRelay_Params{
        public static final String DEVICEID = "deviceid";
        public static final String DEVICENAME = "devicename";
        public static final String VALUE = "value";
    }

    public class updateRelay_Params{
        public static final String DEVICEID = "deviceid";
        public static final String USERPHONE = "userphone";
        public static final String VALUE = "value";
        public static final String RELAY = "relayvalue";
    }
}
