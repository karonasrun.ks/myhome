package com.example.karona.myhome;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    private static final int RegTask =1 ;
    Context context;
    private static final String TAG_PID = "deviceid";
    public String deviceid = "";
    List<Devices> devicesAdapter;
    public RecyclerViewAdapter(List<Devices> devicesAdapter, Context context){
        super();
        this.devicesAdapter = devicesAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_items, parent, false);

        ViewHolder viewHolder = new ViewHolder(v);

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Devices getDataAdapter = devicesAdapter.get(position);
        holder.tvID.setText(getDataAdapter.getId());
        deviceid = getDataAdapter.getId().toString();
        holder.tvUserPhone.setText(getDataAdapter.getUserPhone());
        holder.tvDeviceName.setText(getDataAdapter.getDeviceName());
        holder.tvStatus.setText(getDataAdapter.getValue());
        holder.tvDescription.setText(getDataAdapter.getDescription());
        holder.tvRelayValue.setText(getDataAdapter.getRelayValue());
        holder.tvCreated_at.setText(getDataAdapter.getCreated_at());
        holder.setDevice(getDataAdapter);

    }

    @Override
    public int getItemCount() {

        return devicesAdapter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView tvID;
        public TextView tvUserPhone;
        public TextView tvDeviceName;
        public TextView tvStatus;
        public TextView tvRelayValue;
        public TextView tvDescription;
        public TextView tvCreated_at;
        public TextView buttonViewOption;
        private Devices device;

        public void setDevice(Devices device) {
            this.device = device;
        }

        public ViewHolder(View itemView) {

            super(itemView);

            tvID = (TextView) itemView.findViewById(R.id.tvId) ;
            tvUserPhone = (TextView) itemView.findViewById(R.id.tvuserPhone) ;
            tvDeviceName = (TextView) itemView.findViewById(R.id.tvDeviceName) ;
            tvStatus = (TextView) itemView.findViewById(R.id.tvStatus) ;
            tvRelayValue = (TextView) itemView.findViewById(R.id.tvrelayvalue) ;
            tvDescription = (TextView) itemView.findViewById(R.id.tvDescription) ;
            tvCreated_at = (TextView) itemView.findViewById(R.id.tvcreate_at) ;
            buttonViewOption = (TextView) itemView.findViewById(R.id.textViewOptions);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//                    if(device!=null){
//                        Log.d("device-id",device.getId()+"");
//                        Intent in = new Intent(context,UpdateDeviceActivity.class);
//                        in.putExtra(TAG_PID, device.getId());
//                        context.startActivity(in);
//                    }

                }
            });
            buttonViewOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    //creating a popup menu
                    PopupMenu popup = new PopupMenu(context,buttonViewOption);
                    //inflating menu from xml resource
                    popup.inflate(R.menu.menu);
                    //adding click listener
                    popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                        @Override
                        public boolean onMenuItemClick(MenuItem item) {
                            switch (item.getItemId()) {
                                case R.id.action_edit:
                                    if(device!=null){
                                        Log.d("device-id",device.getId()+"");
                                        Intent in = new Intent(context,UpdateDeviceActivity.class);
                                        in.putExtra(TAG_PID, device.getId());
                                        context.startActivity(in);
                                    }
                                    break;
                                case R.id.action_delete:
                                    if(device!=null){
                                        deleteDevice(device.getId());
                                    }
                                    break;
                            }
                            return false;
                        }
                    });
                    //displaying the popup
                    popup.show();

                }
            });
        }
    }
    private void deleteDevice(String deviceid)
    {
        final HashMap<String, String> map = new HashMap<>();
        map.put(ConnectServer.deleteDevice_Params.DEVICEID, deviceid);
        new AsyncTask<Void, Void, String>() {
            protected String doInBackground(Void[] params) {
                String response = "";
                try {
                    HttpRequest req = new HttpRequest(ConnectServer.ServiceType.DELETE_DEVICE);
                    response = req.prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                } catch (Exception e) {
                    response = e.getMessage();
                }
                return response;
            }

            protected void onPostExecute(String result) {
                //do something with response
                Log.d("on Post Execute:", result);
                onTaskCompleted(result, RegTask);
                notifyDataSetChanged();
            }
        }.execute();
    }

    private void onTaskCompleted(String response, int task) {
        Log.d("responsejson", response.toString());
        ConnectServerUtils.removeSimpleProgressDialog();  //will remove progress dialog
        switch (task) {
            case RegTask:
                    notifyDataSetChanged();
                    Toast.makeText(context, "Device has been deleted successfully!", Toast.LENGTH_SHORT).show();

        }
    }
}
