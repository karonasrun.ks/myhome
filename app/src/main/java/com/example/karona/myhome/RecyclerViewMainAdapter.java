package com.example.karona.myhome;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import java.util.HashMap;
import java.util.List;

public class RecyclerViewMainAdapter extends RecyclerView.Adapter<RecyclerViewMainAdapter.ViewHolder> {

    private static final int RegTask =1 ;
    Context context;
    private static final String TAG_PID = "deviceid";
    public String deviceid = "";
    public String remoteStatus = "";
    public String strRemoteStatus = "";
    List<Devices> devicesAdapter;
    public RecyclerViewMainAdapter(List<Devices> devicesAdapter, Context context){
        super();
        this.devicesAdapter = devicesAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_items_main, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        Devices getDataAdapter = devicesAdapter.get(position);
        holder.tvID.setText(getDataAdapter.getId());
        deviceid = getDataAdapter.getId().toString();
        holder.tvUserPhone.setText(getDataAdapter.getUserPhone());
        holder.tvDeviceName.setText(getDataAdapter.getDeviceName());
        holder.tvStatus.setText(getDataAdapter.getValue());
        remoteStatus = getDataAdapter.getValue().toString();
        if(remoteStatus.equals("ON")) {
            holder.imageView.setImageResource(R.drawable.ic_light_on);
        }else if(remoteStatus.equals("OFF")){
            holder.imageView.setImageResource(R.drawable.ic_light_off);
        }
        holder.tvDescription.setText(getDataAdapter.getDescription());
        holder.tvRelayValue.setText(getDataAdapter.getRelayValue());
        holder.tvCreated_at.setText(getDataAdapter.getCreated_at());
        holder.setDevice(getDataAdapter);
    }

    @Override
    public int getItemCount() {
        return devicesAdapter.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{
        String strON ="ON",strOFF = "OFF";
        String str = "", str_ = "";
        public TextView tvID;
        public TextView tvUserPhone;
        public TextView tvDeviceName;
        public TextView tvStatus;
        public TextView tvRelayValue;
        public TextView tvDescription;
        public TextView tvCreated_at;
        public TextView buttonViewOption;
        public ImageView imageView;
        private Devices device;

        public void setDevice(Devices device) {
            this.device = device;
        }

        public ViewHolder(View itemView) {
            super(itemView);
            tvID = (TextView) itemView.findViewById(R.id.tvId) ;
            tvUserPhone = (TextView) itemView.findViewById(R.id.tvuserPhone) ;
            tvDeviceName = (TextView) itemView.findViewById(R.id.tvDeviceName) ;
            tvStatus = (TextView) itemView.findViewById(R.id.tvStatus) ;
            tvRelayValue = (TextView) itemView.findViewById(R.id.tvrelayvalue) ;
            tvDescription = (TextView) itemView.findViewById(R.id.tvDescription) ;
            tvCreated_at = (TextView) itemView.findViewById(R.id.tvcreate_at) ;
            imageView = (ImageView) itemView.findViewById(R.id.imageView);
            buttonViewOption = (TextView) itemView.findViewById(R.id.textViewOptions);

            buttonViewOption.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    AlertDialog.Builder alertDialog2 = new AlertDialog.Builder(context);
                    if(device.value.toString().equals(strON)) {
                        alertDialog2.setIcon(R.drawable.ic_light_off);
                        str ="បិទ";
                        str_ ="OFF";
                    }
                    else if(device.value.toString().equals(strOFF)){
                        alertDialog2.setIcon(R.drawable.ic_light_on);
                        str = "បើក";
                        str_="ON";
                    }
                    // Setting Dialog Title
                    alertDialog2.setTitle("បញ្ជាក់");
                    // Setting Dialog Message
                    alertDialog2.setMessage("តើលោកអ្នកចង់"+str+"អំពូលភ្លើងដែរឬទេ?");

                    Log.d("","buttonViewOption 3: "+remoteStatus.toString());
                    // Setting Positive "Yes" Btn
                    alertDialog2.setPositiveButton("យល់ព្រម",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Write your code here to execute after dialog
                                    updateRelay(device,device.getId(),device.getUserPhone(),str_.toString(),device.getRelayValue());
                                }
                            });

                    // Setting Negative "NO" Btn
                    alertDialog2.setNegativeButton("បោះបង់",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    // Write your code here to execute after dialog
                                    dialog.cancel();
                                }
                            });

                    // Showing Alert Dialog
                    alertDialog2.show();

                }
            });
        }
    }
    private void updateRelay(final Devices device, String deviceid, String userphone, final String value, String relay)
    {
        final HashMap<String, String> map = new HashMap<>();
        map.put(ConnectServer.updateRelay_Params.DEVICEID, deviceid);
        map.put(ConnectServer.updateRelay_Params.USERPHONE, userphone);
        map.put(ConnectServer.updateRelay_Params.VALUE, value);
        map.put(ConnectServer.updateRelay_Params.RELAY, relay);
        new AsyncTask<Void, Void, String>() {
            protected String doInBackground(Void[] params) {
                String response = "";
                try {
                    HttpRequest req = new HttpRequest(ConnectServer.ServiceType.UPDATE_RALAY);
                    response = req.prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                    Log.d("",response+"");
                } catch (Exception e) {
                    response = e.getMessage();

                }
                return response;
            }

            protected void onPostExecute(String result) {
                //do something with response
                Log.d("on-Post-Execute:", result+","+value);
                device.setValue(value);
                onTaskCompleted(result, RegTask);
            }
        }.execute();
    }

    private void onTaskCompleted(String response, int task) {
        Log.d("responsejson", response.toString());
        ConnectServerUtils.removeSimpleProgressDialog();  //will remove progress dialog
        switch (task) {
            case RegTask:
                //Toast.makeText(context, response+"", Toast.LENGTH_SHORT).show();
                notifyDataSetChanged();
                Toast.makeText(context, "Device has been updated successfully!", Toast.LENGTH_SHORT).show();

        }
    }
}
